/*
    Copyright (C) 2012, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Script container, inside Main, contains Script child
 * 
 * @author Doti
 */
public class ScriptContainer {

	public static final String TEMP_NAME = "TemporaryUnnamedScript";

	public IScript currentScript;
	public String currentScriptFileName;
	public String currentScriptText;
	public boolean forgetScriptAtEnd;

	private List<String> usedFiles;
	public int savedVersion;
	public String savedTitle;
	public String savedSummary;
	public String savedAuthor;
	public String savedStatus;
	public int savedColor;
	public String savedLang;
	public List<String> savedTags;

	public void update(int version, String title, String summary,
			String author, String status, int color, String lang,
			List<String> tags) {
		savedVersion = version;
		savedTitle = title;
		savedSummary = summary;
		savedAuthor = author;
		savedStatus = status;
		savedColor = color;
		savedLang = lang;
		savedTags = tags;
	}

	public String getScriptClassText(boolean testing) {
		try {
			String miniCurrentScriptName = getMiniCurrentScriptName();
			if (miniCurrentScriptName == null)
				miniCurrentScriptName = TEMP_NAME;
			BufferedReader br = new BufferedReader(new InputStreamReader(
					getClass().getResourceAsStream(
							"/ss/FullScript.groovytemplate")));
			String fullScript = "";
			String line; 
			while ((line = br.readLine())!=null) {
				// bug wait() -> IllegalMonitorStateException
				String transformedScriptText = currentScriptText.replaceAll(
						"(\\W)wait\\s*\\(", "$1replacedByWait(");
				line = line.replace("/*scriptText*/", transformedScriptText);				
				line = line.replace("/*scriptNameKeys*/",
						miniCurrentScriptName.replace("/", "."));
				line = line.replace("/*scriptNameJava*/",
						getMiniCurrentScriptNameJava());
				if (testing)
					line = line.replace("/*scriptParentClass*/",
							"ss.desktop.ScriptStub");
				else if(getMachine()==Machine.Android) //is Android
					line = line.replace("/*scriptParentClass*/", "ss.android.Script");
				else
					line = line.replace("/*scriptParentClass*/", "ss.desktop.Script");
				fullScript += line + "\n";
			}
			br.close();
			return fullScript;
		} catch (IOException e) {
			return null;
		}
	}

	public String getMiniCurrentScriptNameJava() {
		String miniCurrentScriptName = getMiniCurrentScriptName();
		if (miniCurrentScriptName == null)
			miniCurrentScriptName = TEMP_NAME;
		return miniCurrentScriptName.replace("/", "_").replaceAll(
				"[^a-zA-Z0-9_]", "");
	}

	public String getMiniCurrentScriptName() {
		if (currentScriptFileName == null)
			return null;
		String res = currentScriptFileName;
		res = res.replace("\\", "/");
		if (res.contains(MainWindow.SCRIPT_FOLDER))
			res = res.substring(res.lastIndexOf(MainWindow.SCRIPT_FOLDER)
					+ MainWindow.SCRIPT_FOLDER.length());
		else if (res.contains("/"))
			res = res.substring(res.lastIndexOf("/") + 1);
		if (res.contains("."))
			res = res.substring(0, res.indexOf("."));
		if (res.contains("_") && res.lastIndexOf("_") > res.length() - 4)
			res = res.substring(0, res.lastIndexOf("_"));
		return res;
	}

	public void readFromFile() throws IOException {
		currentScriptText = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(currentScriptFileName),
				Charset.forName("UTF8")));
		String line;
		while ((line = br.readLine())!=null) {
			currentScriptText += line + "\n";
		}
		br.close();
		if (currentScriptText.startsWith(new String(new byte[] { (byte) 0xEF,
				(byte) 0xBB, (byte) 0xBF }))
				|| currentScriptText.getBytes()[0] == 0x3F
				|| currentScriptText.getBytes()[0] == 0x00)
			currentScriptText = currentScriptText.substring(1);
	}

	public void askForAndImportScript(MainWindow main, String defaultString) {
		try {
			String url = main.getInputString(main.getString("import.url"),
					defaultString);
			if (url != null) {
			    Object[] importResult = importScript(main, url);
				int nbGroovy = (Integer)importResult[0];
				String firstName = (String)importResult[1];
				if(nbGroovy==1)
					main.showMessage(main.getString("import.ok", nbGroovy, " "+firstName), false);
				else
				    main.showMessage(main.getString("import.ok", nbGroovy, ""), false);
			}
		} catch (Exception e) {
			String message = e.toString();
			if(message.contains("429"))
				message += " (download quotas)";
			main.showMessage(main.getString("error", message), true);
		}
		main.setWaiting(false);
	}

	/**
	 * @return array(number of script (.groovy) files, name of the first one)
	 * 
	 */
	public Object[] importScript(MainWindow main, String url) throws IOException,
			MalformedURLException, FileNotFoundException {
		main.setWaiting(true);
		if (!url.contains("://")) {
			if (!url.startsWith("/"))
				url = "/" + url;
			url = MainWindow.WEBSITE_SECURE_URL + url;
		}
		
		URLConnection conn = null;
		if(url.startsWith("http")) {
			WebCommunicator webCommunicator = new WebCommunicator();
			boolean noMoreRedirects = false;
			int redirects = 0;
			HttpURLConnection httpConn = null;
			while (!noMoreRedirects && redirects < 16) {
				httpConn = webCommunicator.getConnection(url);
				noMoreRedirects = true;
				if (httpConn.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM
						|| httpConn.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP
						|| httpConn.getResponseCode() == HttpURLConnection.HTTP_SEE_OTHER) {
					url = httpConn.getHeaderField("Location");
					noMoreRedirects = false;
					redirects++;
				}
			}
			conn = httpConn;
		} else { //file
			conn = new URL(url).openConnection();				
		}
		
		ZipInputStream zip = new ZipInputStream(conn.getInputStream());
		ZipEntry ze;
		int nbGroovy = 0;
		String firstName = null;
		while ((ze = zip.getNextEntry()) != null) {
			File f = new File(main.getDataFolder(), ze.getName());
			if (ze.isDirectory())
				f.mkdirs();
			else {
				boolean savePrevious = f.exists();
				File pf = null;
				if(savePrevious) {
					File pfd = new File(main.getDataFolder(), "previous");
					pf = new File(pfd, ze.getName());
					pf.getParentFile().mkdirs();
					FileInputStream fis = new FileInputStream(f);
					FileOutputStream fos = new FileOutputStream(pf);
					fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
					fos.close();
					fis.close();
				}
				FileOutputStream fos = new FileOutputStream(f);
				int o;
				while ((o = zip.read()) > -1)
					fos.write(o);
				fos.close();
				zip.closeEntry();
				if (savePrevious) {
					boolean same = true;
					if(f.length() != pf.length())
						same = false;
					//small file : lets compare details of files
					if(same && f.length() < 20000) {
						FileInputStream fis1 = new FileInputStream(f);
						FileInputStream fis2 = new FileInputStream(pf);
						int v1 = 0;
						while((v1 = fis1.read())!=-1) {
							int v2 = fis2.read();
							if(v1 != v2) {
								same = false;
								break;
							}
						}
						fis2.close();
						fis1.close();
					}
					if(same) {
						pf.delete();
					}
				}
				if (ze.getName().endsWith(".groovy")) {
					nbGroovy++;
					if(firstName == null)
					    firstName = ze.getName();
				}
			}
		}
		zip.close();
		return new Object[]{ new Integer(nbGroovy), firstName};
	}

	public void clearUsedFiles() {
		usedFiles = new ArrayList<String>();
	}

	public void addToMainUsedFiles(String fileName) {
		if (fileName != null) {
			String fullFileName = new File(fileName).getAbsolutePath().replace(
					"\\.\\", "\\");
			if (!usedFiles.contains(fullFileName))
				usedFiles.add(fullFileName);
		}
	}

	public List<String> getUsedFiles() {
		return usedFiles;
	}

	public Machine getMachine() {
		try {
			Class.forName("android.R");
			return Machine.Android;
		} catch(ClassNotFoundException e) {
			return Machine.Desktop;
		}
	}
	
	public enum Machine { Desktop, Android };
}
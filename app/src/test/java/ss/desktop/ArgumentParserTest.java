/*
    Copyright (C) 2017, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ArgumentParserTest {

	/**
	 * Test method for {@link ss.desktop.ArgumentParser#ArgumentParser(java.lang.String[])}.
	 */
	@Test
	public void testArgumentParser() {
		ArgumentParser ap;
		ap = new ArgumentParser(
				new String[]{}
		);
		assertEquals(0, ap.size());
		ap = new ArgumentParser(
				new String[]{"default1","default2","-x","-y=1","-z=2=3"}
		);
		assertEquals("default2", ap.get("default"));
		assertEquals("true", ap.get("x"));
		assertEquals("1", ap.get("y"));
		assertEquals("2=3", ap.get("z"));
	}

}

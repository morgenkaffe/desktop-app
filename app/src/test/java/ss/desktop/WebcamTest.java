package ss.desktop;

import com.github.sarxos.webcam.Webcam;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import static org.junit.jupiter.api.Assertions.*;

public class WebcamTest {
	/**
	 * Test that we can capture a webcam.
	 * 
	 * To setup a virtual webcam on Debian do the following:
	 * 
	 * - sudo apt-get update && sudo apt-get install -y ffmpeg v4l2loopback-dkms
     * - sudo modprobe v4l2loopback video_nr=42
     * - ffmpeg -f lavfi  -re -i rgbtestsrc=s=640x480  -f v4l2 -vcodec rawvideo -pix_fmt yuv420p /dev/video42
	 * 
	 * And then run the tests with the environment variable TEST_WEBCAM set to true.
	 * 
	 * This can't be run on Gitlab CI/CD because the docker container can't load custom kernel modules.
	 * 
	 * @throws IOException
	 */
	@Test
	@EnabledIfEnvironmentVariable(named = "TEST_WEBCAM", matches = "true", 
		disabledReason = "Only run this test on systems with (virtual) webcam available")
	public void testWebcams() throws IOException {
		assertEquals(1, Webcam.getWebcams().size());
		
		Webcam webcam = Webcam.getDefault();
		
		assertTrue(webcam.open());
		
		BufferedImage actualImage = webcam.getImage();
		
		assertTrue(webcam.close());

		assertNotNull(actualImage);
		
		ImageIO.write(actualImage, "JPEG", new File("testtarget.jpg"));
	}
}

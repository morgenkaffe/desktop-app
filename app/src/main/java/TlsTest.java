import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class TlsTest {

	public static void main(String[] args) throws Exception {
		
		SSLContext sslCtx = SSLContext.getInstance("TLS");
		TrustManager[] trustManagers = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		sslCtx.init(null, trustManagers, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslCtx
				.getSocketFactory());
		
		javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
				new javax.net.ssl.HostnameVerifier(){

				    public boolean verify(String hostname,
				            javax.net.ssl.SSLSession sslSession) {
				        return hostname.equals(sslSession.getPeerHost());
				    }
				});
		
		
//		System.setProperty("jsse.enableSNIExtension", "false");
		
		//new URL("https://services.temporarily.exposed").openConnection().getInputStream().close();
		
		new URL("https://play-link.com").openConnection().getInputStream().close();
	}

}

/*
    Copyright (C) 2011, 2015, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Desktop.Action;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.security.InvalidParameterException;
import java.util.*;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.swing.UIManager;

import com.github.sarxos.webcam.Webcam;

import ss.IScript;
import ss.MainWindow;
import ss.Parameters;
import ss.PropertiesWorker;
import ss.ScriptContainer;
import ss.TextSource;
import ss.WebCommunicator;

// TODO javazoom doesn't seem to exist anymore. Might be necessary to replace this library.
import javazoom.jl.player.Player;

/**
 * <p>
 * This class is extended by scripts (they are put inside run()).
 * </p>
 * <p>
 * Refactoring goal : most of main become private, reducing ui process here, by
 * ....FromScript() functions, and other specialized classes.
 * </p>
 * 
 * @author Doti
 */
public class Script implements IScript {

	private static final int SOUND_BUFFER_SIZE = 4096;
	// let's say the usual fps is 22050 or better
	public static final int WAIT_SOUND_THREAD_MS = 1000 * SOUND_BUFFER_SIZE / 4 / 22050;
	//private static final int SYNC_DELAY_MS = 500;
	private static final int SYNC_DELAY_MS = 200;
	private static final String[] IMAGE_EXTENSIONS = new String[] { "gif",
			"png", "bmp", "jpeg", "jpg" };
	private static final String CAPTURED_IMAGE_FOLDER = "captured/";
	private static final String RECEIVED_IMAGE_FOLDER = "received/";
	private static final int WEBCAM_IDEAL_WIDTH = 800;

	protected MainWindow mainWindow;
	protected TextSource textSource;
	private List<Thread> soundThreads;
	private List<SourceDataLine> soundLines;
	private boolean shouldStopSoundThreads;
	private WebCommunicator webCommunicator;

	private Map<String, byte[]> buffers;

	public Script(MainWindow mainWindow, TextSource textSource) {
		this.mainWindow = mainWindow;
		this.textSource = textSource;
		soundThreads = new ArrayList<Thread>();
		soundLines = new ArrayList<SourceDataLine>();
		shouldStopSoundThreads = false;
		webCommunicator = new WebCommunicator();
		ScriptContainer sc = mainWindow.getScriptContainer();
		if (mainWindow != null)
			sc.clearUsedFiles();
		buffers = new HashMap<String, byte[]>();
		if (mainWindow != null && sc.currentScriptFileName != null)
			addToMainUsedFiles(sc.currentScriptFileName);
	}

	public Script(MainWindow mainWindow, TextSource textSource, long seed) {
		throw new RuntimeException("Not implemented");
	}

	public void finalize() {
		stopSoundThreads();
	}

	@Override
	@SuppressWarnings("deprecation")
	public void stopSoundThreads() {
		shouldStopSoundThreads = true;
		synchronized (soundLines) {
			for (SourceDataLine sd : soundLines)
				sd.close();
			soundLines.clear();
		}
		try {
			Thread.sleep(WAIT_SOUND_THREAD_MS);
		} catch (InterruptedException e) {
		}
		synchronized (soundThreads) {
			for (Thread t : soundThreads)
				if (t.isAlive())
					t.stop();
			soundThreads.clear();
		}
		shouldStopSoundThreads = false;
		mainWindow.setPlayingSound(false);
	}

	public void setInfos(int version, String title, String summary,
			String author, String status, int color, String lang,
			List<String> tags) {
		mainWindow.changeColor(color);
		mainWindow.setTitle(title);
		mainWindow.getScriptContainer().update(version, title, summary, author,
				status, color, lang, tags);
		if (version > MainWindow.API_VERSION) {
			mainWindow.showMessage(textSource.getString("error.version",
					version, MainWindow.API_VERSION), true);
		}
	}

	public void show(Object s) {
		String text2 = null;
		if (s != null)
			text2 = String.valueOf(s);
		mainWindow.showFromScript(text2);
	}

	public double showButton(Object s, double duration) {
		try {
			return mainWindow.showButtonFromScript(String.valueOf(s), duration);
			//TODO throw this exception, catch in main 
		} catch (InterruptedException e) { 
			return 0;
		}
	}

	public double showButton(Object s) {
		return showButton(s, 30 * 24 * 3600);
	}

	public float showPopup(Object text) {
		String text2 = null;
		if (text != null)
			text2 = String.valueOf(text);
		return mainWindow.showPopupFromScript(text2);
	}

	public String getString(Object text, Object defaultValue) {
		String text2 = null;
		if (text != null)
			text2 = String.valueOf(text);
		try {
			return mainWindow.getStringFromScript(text2,
					String.valueOf(defaultValue));
		} catch (InterruptedException e) {
			return null;
		}
	}

	public Integer getInteger(Object text, Integer defaultValue) {
		Integer res = null;
		do {
			String defaultValue2 = null;
			if (defaultValue != null)
				defaultValue2 = String.valueOf(defaultValue);
			String textRes = getString(text, defaultValue2);
			try {
				res = new Integer(textRes);
			} catch (Exception e) {
			}
		} while (res == null);
		return res;
	}

	public Float getFloat(Object text, Float defaultValue) {
		Float res = null;
		do {
			String defaultValue2 = null;
			if (defaultValue != null)
				defaultValue2 = String.valueOf(defaultValue);
			String textRes = getString(text, defaultValue2);
			try {
				res = new Float(textRes);
			} catch (Exception e) {
			}
		} while (res == null);
		return res;
	}

	public boolean getBoolean(Object text, Object yesMessage, Object noMessage) {
		String text2 = null;
		if (text != null)
			text2 = String.valueOf(text);
		try {
			return mainWindow.getBooleanFromScript(text2,
					String.valueOf(yesMessage), String.valueOf(noMessage));
		} catch (InterruptedException e) {
			return false;
		}
	}

	public boolean getBoolean(Object text) {
		return getBoolean(text,
				(String) UIManager.get("OptionPane.yesButtonText"),
				(String) UIManager.get("OptionPane.noButtonText"));
	}

	public int getSelectedValue(Object text, List<String> values) {
		String text2 = null;
		if (text != null)
			text2 = String.valueOf(text);
		try {
			return mainWindow.getSelectedValueFromScript(text2, values);
		} catch (InterruptedException e) {
			return 0;
		}
	}

	public List<Boolean> getBooleans(Object title, List<String> values,
			List<Boolean> defaultValues) {
		String title2 = null;
		if (title != null)
			title2 = String.valueOf(title);
		GraphicsDevice device = GraphicsEnvironment
				.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if (device.getFullScreenWindow() == mainWindow)
			device.setFullScreenWindow(null);		
		JOptionDialog jod = new JOptionDialog((MainFrame) mainWindow,
				textSource.getString("title"), title2, values, defaultValues);
		return jod.getModalResult();
	}

	public int getRandom(Integer max) {
		if (max == null)
			max = 100;
		return (int) (Math.random() * max);
	}
	
	public int getRandom(double max) {
	    return getRandom((int) max);
	}

	public void replacedByWait(double duration) {
		try {
			mainWindow.waitFromScript(duration);
		} catch (InterruptedException e) {
			
		}
	}

	public void waitWithGauge(double duration) {
		try {
			mainWindow.waitWithGaugeFromScript(duration);
		} catch (InterruptedException e) {
			
		}
	}
	
	public String getDataFolder() {
	    String s = mainWindow.getDataFolder().getAbsolutePath()+"/";
	    if(s.endsWith("./"))
	    	s = s.substring(0, s.length()-2);
	    return s;
	}

	public int getTime() {
		return (int) (Math.round((System.currentTimeMillis()) / 1000.0));
	}

	public String getImage(Object message) {
		Webcam webcam = null;
		try {
		    Integer webcamIndex = mainWindow.getPropertiesWorker().loadInteger("application.webcam.index");
			if(webcamIndex!=null && webcamIndex>-1) {
				webcam = Webcam.getWebcams(5000).get(webcamIndex);
			} else {
				webcam = Webcam.getDefault(5000);
			}
			if (webcam == null)
				return getFile(message);
			Dimension[] viewSizes = webcam.getViewSizes();
			int minDiff = 100000;
			Dimension best = null;
			for (Dimension viewSize : viewSizes) {
				int diff = Math.abs(viewSize.width - WEBCAM_IDEAL_WIDTH);
				if (diff < minDiff) {
					minDiff = diff;
					best = viewSize;
				}
			}
			webcam.setViewSize(best);
			webcam.open();
			String filename = (int) (Math.random() * Integer.MAX_VALUE) + ".jpeg";
			File fullfile = new File(MainWindow.IMAGE_FOLDER
					+ CAPTURED_IMAGE_FOLDER, filename);
			new File(MainWindow.IMAGE_FOLDER + CAPTURED_IMAGE_FOLDER).mkdirs();
			ImageIO.write(webcam.getImage(), "JPEG", fullfile);
			if (fullfile.exists())
				return fullfile.getCanonicalPath();
			else
				return null;
		} catch (Exception e) {
			return getFile(message);
		} finally {
			if(webcam!=null && webcam.isOpen())
				webcam.close();
		}
	}

	public String getFile(Object message) {
		File srcFile = mainWindow.getFileFromChooser(false, null, String.valueOf(message), null);
		if (srcFile != null) {
			try {
				return srcFile.getCanonicalPath();
			} catch (IOException e) {
				return null;
			}
		} else {
			return null;
		}
	}

	public String loadString(String key) {
		return mainWindow.getPropertiesWorker().loadString(key);
	}

	public Integer loadInteger(String key) {
		return mainWindow.getPropertiesWorker().loadInteger(key);
	}

	public Float loadFloat(String key) {
		return mainWindow.getPropertiesWorker().loadFloat(key);
	}

	public Boolean loadBoolean(String key) {
		return mainWindow.getPropertiesWorker().loadBoolean(key);
	}

	public String loadFirstTrue(String... keys) {
		return mainWindow.getPropertiesWorker().loadFirstTrue(keys);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, ?> loadMap(String key) {
		return (Map<String, ?>)load(key);
	}
	
	public Object load(String key) {
		return mainWindow.getPropertiesWorker().load(key);
	}

	
	public void save(String key, Object v) {
		mainWindow.getPropertiesWorker().save(key, v);
	}

	public void setImage(String fileName) {
		try {
			if (fileName == null || fileName.trim().isEmpty()) {
				mainWindow.setImageFromScript((String) null);
			} else {
				File f;
				if (fileName.startsWith("/") || fileName.contains(":"))
					f = new File(fileName);
				else {
					f = new File(mainWindow.getDataFolder(),
							MainWindow.VIDEO_FOLDER + fileName);
					if (!f.exists()) {
						f = new File(mainWindow.getDataFolder(),
								MainWindow.IMAGE_FOLDER + fileName);
					}
				}
				if (!f.exists()) {
					showPopup(textSource.getString("missing", f.getPath()));
				} else {
					addToMainUsedFiles(f.getPath());

					String extension = f.getPath().substring(
							f.getPath().lastIndexOf(".") + 1);
					if (Arrays.asList(IMAGE_EXTENSIONS).contains(extension)) {
						mainWindow.setImageFromScript(f.getPath());
					} else if (Arrays.asList(
							new String[] { "mpg", "mpeg", "avi", "asf", "h264",
									"rv", "mov", "wmf", "3gp" }).contains(
							extension)) {
						useFile(f.getCanonicalPath());
					}

				}
			}
		} catch (Exception e) {
			showPopup(textSource.getString("error", e.toString()));
		}
	}

	public void setImage(byte[] bytes, int useless) {
		mainWindow.setImageFromScript(bytes);
	}

	// depends on AWT; not in Android
	public void setImage(Image image, boolean unu) {
		ScriptContainer sc = mainWindow.getScriptContainer();
		if (sc.savedVersion > 4)
			throw new RuntimeException("Deprecated");
		// Image -> byte[]
		int width = image.getWidth(null);
		int height = image.getHeight(null);
		int type = BufferedImage.TYPE_INT_ARGB;
		BufferedImage bi = new BufferedImage(width, height, type);
		Graphics2D g2d = bi.createGraphics();
		g2d.drawImage(image, 0, 0, null);
		g2d.dispose();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(bi, "png", baos);
		} catch (IOException e) {
		}
		mainWindow.setImageFromScript(baos.toByteArray());
	}

	public void useFile(String fileName) {
		File f;
		f = new File(fileName);
		boolean succeed = false;
		if (Desktop.isDesktopSupported()) {
			Desktop desktop = Desktop.getDesktop();
			try {
				desktop.open(f);
				succeed = true;
			} catch (IOException e) {
			}
		}
		if (!succeed) {
			String command = f.getAbsolutePath();
			String osname = System.getProperty("os.name").toLowerCase();
			if (osname.contains("windows 9"))
				// TODO test
				command = "command.com /C start \"\" \"" + f.getAbsolutePath()
						+ "\"";
			else if (osname.contains("windows"))
				command = "cmd /C start \"\" \"" + f.getAbsolutePath() + "\"";
			else if (osname.contains("mac"))
				// TODO test
				command = "open '" + f.getAbsolutePath() + "'";
			else if (osname.contains("linux"))
				// TODO test
				command = "xdg-open '" + f.getAbsolutePath() + "'";
			try {
				Process p = Runtime.getRuntime().exec(command);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}
				int exitValue = 0;
				try {
					exitValue = p.exitValue();
				} catch (IllegalThreadStateException e) {
				}
				if (exitValue != 0) {
					String errorStr = "";
					BufferedReader br = new BufferedReader(
							new InputStreamReader(p.getErrorStream()));
					while (br.ready())
						errorStr += br.readLine();
					br.close();
					br = new BufferedReader(new InputStreamReader(
							p.getInputStream()));
					while (br.ready())
						errorStr += br.readLine();
					br.close();
					showPopup(fileName
							+ " could not be open.\nThe command was : "
							+ command + ", the exit value " + exitValue
							+ " and the error was :\n " + errorStr);
				}
				succeed = true;
			} catch (IOException e) {
				showPopup(textSource.getString("error",
						command + " " + e.toString()));
			}
		}
	}

	public void useUrl(String url) {
		Desktop desktop = Desktop.getDesktop();
		try {
			desktop.browse(new URI(url));
		} catch (Exception e) {
			showPopup(textSource.getString("error", url + " " + e.toString()));
		}
	}

	public void useEmailAddress(String address) {
		Desktop desktop = Desktop.getDesktop();
		try {
			if (desktop.isSupported(Action.MAIL)) {
				if (!address.startsWith("mailto:"))
					address = "mailto:" + address;
				desktop.mail(new URI(address));
			} else
				show("(no mail client configured ; send a mail to " + address
						+ ")");
		} catch (Exception e) {
			showPopup(textSource.getString("error",
					address + " " + e.toString()));
		}

	}

	public void playSound(String fileName) {
		if (fileName == null) {
			stopSoundThreads();
			return;
		}
		if (fileName.toLowerCase().endsWith("mp3"))
			playMp3Sound(fileName);
		else
			playWavSound(fileName);
	}

	public void playBackgroundSound(String fileName) {
		playBackgroundSound(fileName, 1);
	}
	
	public void playBackgroundSound(String fileName, int times) {
		if (fileName == null) {
			stopSoundThreads();
			return;
		}
		if (!loadInMemory(fileName))
			return;
		Thread t = new Thread() {
			private String fileName;
			private int times;

			public Thread init(String fileName, int times) {
				this.fileName = fileName;
				this.times = times;
				return this;
			}

			public void run() {
				for(int i=0;i<times;i++)
					playSound(fileName);
			}
		}.init(fileName, times);
		synchronized (soundThreads) {
			soundThreads.add(t);
		}
		t.start();
	}

	private void playWavSound(String fileName) {
		try {
			long startTime = System.currentTimeMillis();
			if (!loadInMemory(fileName))
				return;
			InputStream is = new ByteArrayInputStream(buffers.get(fileName));
			AudioInputStream ais = AudioSystem.getAudioInputStream(is);
			DataLine.Info info = new DataLine.Info(SourceDataLine.class,
					ais.getFormat());
			SourceDataLine dl = (SourceDataLine) AudioSystem.getLine(info);
			synchronized (soundLines) {
				soundLines.add(dl);
			}
			dl.open(ais.getFormat());
			mainWindow.setPlayingSound(true);
			dl.start();
			// sync problems on audio line
			// about other tries : LineListener make blocking line ;
			// thread.sleep may be as efficient as this
			long elapsed = System.currentTimeMillis() - startTime;
			int bytesPerSeconds = (int) (ais.getFormat().getFrameSize() * ais
					.getFormat().getFrameRate());
			byte[] buf = new byte[SOUND_BUFFER_SIZE];
			Arrays.fill(buf, (byte) 0);
			if (elapsed < SYNC_DELAY_MS)
				for (int i = 0; !shouldStopSoundThreads
						&& i < (SYNC_DELAY_MS - elapsed) * bytesPerSeconds
								/ 1000 / SOUND_BUFFER_SIZE; i++)
					dl.write(buf, 0, SOUND_BUFFER_SIZE);
			int n = 0;
			while (!shouldStopSoundThreads && (n = ais.read(buf)) > -1)
				dl.write(buf, 0, n);

			dl.drain();
			dl.close();
			synchronized (soundLines) {
				soundLines.remove(dl);
			}
			ais.close();
			updatePlayingSound(Thread.currentThread());
		} catch (Exception e) {
			showPopup(textSource.getString("error", e.toString()));
		}
	}

	private void playMp3Sound(String fileName) {
		try {
			if (!loadInMemory(fileName))
				return;
			long startTime = System.currentTimeMillis();
			InputStream is = new ByteArrayInputStream(buffers.get(fileName));
			Player player = new Player(is);
			mainWindow.setPlayingSound(true);
			long elapsed = System.currentTimeMillis() - startTime;
			if (elapsed < SYNC_DELAY_MS)
				Thread.sleep(SYNC_DELAY_MS - elapsed);
			player.play();
			player.close();
			updatePlayingSound(Thread.currentThread());
		} catch (Exception e) {
			showPopup(textSource.getString("error", e.toString()));
		}
	}

	public void openCdTrays() {
		String osname = System.getProperty("os.name").toLowerCase();
		try {
			if (osname.contains("windows")) {
				File file;
				file = File.createTempFile("sexscriptscd", ".vbs");
				file.deleteOnExit();
				FileWriter fw = new java.io.FileWriter(file);
				String vbs = "Set oWMP = CreateObject(\"WMPlayer.OCX\")\n"
						+ "Set colCDROMs = oWMP.cdromCollection\n"
						+ "For d = 0 to colCDROMs.Count - 1\n"
						+ "colCDROMs.Item(d).Eject\n" + "Next\n"
						+ "set owmp = nothing\n" + "set colCDROMs = nothing\n"
						+ "wscript.Quit(0)";
				fw.write(vbs);
				fw.close();
				Runtime.getRuntime().exec("wscript " + file.getPath())
						.waitFor();
			} else if (osname.contains("mac")) {
				Runtime.getRuntime().exec("drutil tray eject");
			} else {
				Runtime.getRuntime().exec("eject");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param fileName
	 *            unformatted file name
	 * @return success
	 */
	private boolean loadInMemory(String fileName) {
		if (buffers.containsKey(fileName))
			return true;
		File f;
		if (fileName.startsWith("/") || fileName.contains(":"))
			f = new File(fileName);
		else
			f = new File(mainWindow.getDataFolder(), MainWindow.SOUND_FOLDER
					+ fileName);
		if (!f.exists()) {
			showPopup(textSource.getString("missing", f.getPath()));
			return false;
		}
		try {
			FileInputStream is = new FileInputStream(f);
			byte[] bs = new byte[(int) f.length() + 1024];
			int n = 0;
			int n2;
			while ((n2 = is.read(bs, n, 1024)) > -1)
				n += n2;
			is.close();
			buffers.put(fileName, bs);
			addToMainUsedFiles(f.getPath());
			return true;
		} catch (Exception e) {
			showPopup(textSource.getString("missing", f.getPath()));
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	public void exit() {
		mainWindow.dispose();
		Thread.currentThread().stop();
	}

	@Override
	public boolean isConnected() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("action", "test");
		try {
			String s = webCommunicator.send(data, null).trim();
			return s.equals("ok");
		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public void send(String key, Object value) {
		if(key.length()>Parameters.SSCA_KEY_LIMIT_LENGTH) {
			throw new InvalidParameterException("Key "+key.substring(0,10)+"... too long");
		}
		
		Map<String, String> props;
		
		String valueStr;
		if(value!=null) {
			valueStr = String.valueOf(value); 
			if(valueStr.length()>Parameters.SSCA_VALUE_LIMIT_LENGTH) {
				throw new InvalidParameterException("Value "+valueStr.substring(0,10)+"... too long");
			}
			 props = PropertiesWorker.getPropertiesListFromObjectRec(key, value);
		} else {
			props = new HashMap<String, String>();
		}
		Map<String, String> data = new HashMap<String, String>();
		data.put("action", "ssca_send_mlt");
		data.put("basekey", key);
		for(String detailKey:props.keySet()) {
			data.put("data["+detailKey+"]", props.get(detailKey));							
		} 
		
		try {
			System.out.println(webCommunicator.send(data, null));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Boolean receiveBoolean(String key) {
		try {
			return (Boolean)receive(key);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@Override
	public Float receiveFloat(String key) {
		try {
			return (Float)receive(key);
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public Integer receiveInteger(String key) {
		try {
			return (Integer)receive(key);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, ?> receiveMap(String key) {
		return (Map<String, ?>)receive(key);
	}
	
	@Override
	public String receiveString(String key) {
		Object o = receive(key);
		if(o==null)
			return null;
		else
			return String.valueOf(o);
	}
	
	@Override
	public Object receive(String key) {
		Map<String, String> data = new HashMap<String, String>();
		data.put("action", "ssca_receive_mlt");
		data.put("key", key);
		try {
			String s = webCommunicator.send(data, null);
			Properties props = new Properties();
			props.load(new StringReader(s));
			if(props.containsKey("error"))
				throw new InvalidParameterException("Key "+key+" : "+props.getProperty("error"));
			return PropertiesWorker.getObjectFromPropertiesListRec(key, props);
		} catch (IOException ex) {
			return null;
		}
	}	

	@Override
	public String sendImage(String filename) {
		if (!new File(filename).exists())
			return null;
		String ext = filename.substring(filename.lastIndexOf(".") + 1);
		if (!Arrays.asList(IMAGE_EXTENSIONS).contains(ext.toLowerCase()))
			return null;
		Map<String, String> data = new HashMap<String, String>();
		data.put("action", "ssca_sendimage");
		List<String> files = new ArrayList<String>();
		files.add(filename);
		try {
			// receive "Ok codename1.png,codename2.jpeg,..."
			String res = webCommunicator.send(data, files);
			if (res.startsWith("Ok"))
				return res.substring(res.indexOf(' ') + 1).split(",")[0].trim();
			else
				return null;
		} catch (IOException ex) {
			System.out.println(ex);
			return null;
		}
	}

	@Override
	public String receiveImage(String code) {
		String ext = code.substring(code.lastIndexOf(".") + 1);
		if (!Arrays.asList(IMAGE_EXTENSIONS).contains(ext.toLowerCase()))
			return null;
		if (new File(Parameters.IMAGE_FOLDER + RECEIVED_IMAGE_FOLDER + code).exists())
			return RECEIVED_IMAGE_FOLDER + code;
		try {
			Map<String, String> data = new HashMap<String, String>();
			data.put("action", "ssca_receiveimage");
			data.put("code", code);
			new File(MainWindow.IMAGE_FOLDER + RECEIVED_IMAGE_FOLDER).mkdirs();
			if (webCommunicator.sendAndSaveToFile(data, null,
					Parameters.IMAGE_FOLDER + RECEIVED_IMAGE_FOLDER + code) < 10)
				return null;
			else
				return RECEIVED_IMAGE_FOLDER + code;
		} catch (IOException ex) {
			return null;
		}
	}

	@Override
	public String run() {
		// override me !
		return null;
	}

	private void addToMainUsedFiles(String fileName) {
		mainWindow.getScriptContainer().addToMainUsedFiles(fileName);
	}

	private void updatePlayingSound(Thread closingThread) {
		boolean playingSound = false;
		synchronized (soundThreads) {
			if (soundThreads != null)
				for (Thread t : soundThreads)
					if (t.isAlive() && t != closingThread)
						playingSound = true;
		}
		mainWindow.setPlayingSound(playingSound);
	}
}

/*
    Copyright (C) 2011, 2017, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.desktop;

import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Date;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 * <p>
 * Starts class, tests, hide console then show main frame
 * </p>
 * <p>
 * Refactoring goal : only do things above.
 * </p>
 * 
 * @author Doti
 */
public class Main {

	public static final boolean DEBUG_MODE = true; 
	
	public static void main(String[] args) {
		if(new ArgumentParser(args).containsKey("h") || new ArgumentParser(args).containsKey("help") || 
				new ArgumentParser(args).containsKey("?")) {
			System.out.println("Usage : [command] [options] [script]\n"+
					"Options :\n"+
					"-h|-help|-? : this help\n"+
					"-s=zzz|-script=zzz : startup script\n"+
					"-d=zzz|data=zzz : property file\n"+
					"-t|-discreet : discreet mode\n"+
					"Examples :\n"+
					"./sexscript -h\n"+
					"./sexscript intro\n"+
					"./sexscript -s=intro -d=data2.properties");
		} 
		if (!quickTest()) 
			return;
				
		if(!DEBUG_MODE)
			neuterConsole();
		setLookAndFeel(true);
		new MainFrame(args);
	}

	private static void neuterConsole() {
		try {
			// neutering console writings
			System.setOut(new PrintStream(new OutputStream() {
				@Override
				public void write(int b) throws IOException {
				}
			}));
			System.setErr(new PrintStream(new OutputStream() {
				String s = "";

				@Override
				public void write(int b) throws IOException {
					if (b >= 0)
						s += ((char) b);
					if ((char) b == '\n') {
						if (s.contains("Exception") && !s.startsWith(" ")
								&& !s.toLowerCase().contains("caused by")) {
							s = new Date().toString() + "\n"
									+ getAdditionnalErrorMessage() + s + "\n";
						}
						Writer w = new FileWriter("errors.log", true);
						w.write(s);
						w.close();
						s = "";
					}
				}
			}));
		} catch (Exception e) {
		}
	}

	private static boolean quickTest() {
		try {
			File f = new File("test");
			Writer w = new FileWriter(f);
			w.write(0);
			w.close();
			f.delete();
		} catch (Throwable e) {
			String eStr = e.toString();
			if (e.getCause() != null)
				eStr += "\nCaused by : " + e.getCause().toString();
			JOptionPane.showMessageDialog(null, "Error !\n" + eStr + "\n"
					+ "Make sure you extracted the ZIP file, not running this "
					+ "app from it, and writing to its directory is allowed.\n"
					+ "Helpful system informations to resolve "
					+ "it on the forum :\n" + getAdditionnalErrorMessage()
					+ "file.encoding : " + System.getProperty("file.encoding")
					+ "\n" + "language : " + Locale.getDefault().getLanguage()
					+ "_" + Locale.getDefault().getCountry());
			return false;
		}
		try {
			new GroovyShell();
		} catch (Throwable e) {
			String eStr = e.toString();
			if (e.getCause() != null)
				eStr += "\nCaused by : " + e.getCause().toString();
			JOptionPane.showMessageDialog(null, "Groovy error !\n" + eStr
					+ "\n" + "Helpful system informations to resolve "
					+ "it on the forum :\n" + getAdditionnalErrorMessage()
					+ "file.encoding : " + System.getProperty("file.encoding")
					+ "\n" + "language : " + Locale.getDefault().getLanguage()
					+ "_" + Locale.getDefault().getCountry());
			return false;
		}
		return true;
	}

	/**
	 * Problem : JFileChooser / bug sun-4711700 => may change sometime to metal,
	 * then back
	 */
	public static void setLookAndFeel(boolean toSystemLF) {
		try {
			String className;
			if (toSystemLF)
				className = UIManager.getSystemLookAndFeelClassName();
			else
				className = "javax.swing.plaf.metal.MetalLookAndFeel";
			if (!UIManager.getLookAndFeel().getClass().getName()
					.equals(className))
				UIManager.setLookAndFeel(className);
		} catch (Exception ex) {
			if (ex instanceof RuntimeException)
				throw (RuntimeException) ex;
			else
				ex.printStackTrace();
		}
	}

	public static String getAdditionnalErrorMessage() {
		return "os.name : " + System.getProperty("os.name") + "\n"
				+ "os.arch : " + System.getProperty("os.arch") + "\n"
				+ "java.version : " + System.getProperty("java.version") + "\n";
	}
}

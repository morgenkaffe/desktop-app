/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

/**
 * Returns : chosen scriptName
 *
 * @author Doti
 */
public class ImportActivity extends ListActivity {

	private List<Map<String, String>> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		list = new ArrayList<Map<String, String>>();
		try {
			String jsonFilename = getIntent().getStringExtra("jsonFilename");
			BufferedReader br = new BufferedReader(new FileReader(jsonFilename));
			String jsonList = "";
			String line;
			while((line = br.readLine())!=null)
				jsonList += line+"\n";
			br.close();
			JSONArray ja = new JSONArray(jsonList);
			DateFormat dt = DateFormat.getDateInstance();
			for (int i = 0; i < ja.length(); i++) {
				JSONObject jo = ja.getJSONObject(i);
				Map<String, String> m = new HashMap<String, String>();
				m.put("url", jo.getString("url"));
				m.put("title", jo.getString("script_title"));
				m.put("summary", jo.getString("script_summary"));
				m.put("author", jo.getString("script_author"));
				m.put("language", jo.getString("script_language"));
				m.put("tags", jo.getString("script_tags"));
				m.put("status", jo.getString("script_status"));
				m.put("modification_date", dt.format(
						new Date(1000 * Long.parseLong(jo.getString("script_modificationdate")))));
				if (Integer.parseInt(jo.getString("script_validated")) == 1)
					m.put("validated_text", "Validated");
				else
					m.put("validated_text", "");
				list.add(m);
			}
			Log.d("ImportActivity", "onCreate() : " + list.size() + " items");
		} catch (Exception e) {
			Log.e("ImportActivity", "Error : " + e);
		}
		setListAdapter(new ColorfullSimpleAdapter(this, list, R.layout.import_list_item,
				new String[]{"title", "author","summary", "language", "tags", "status",
						"validated_text","modification_date"}, new int[]{R.id.import_title,
				R.id.import_author, R.id.import_summary, R.id.import_language, R.id.import_tags,
				R.id.import_status, R.id.import_validated_text, R.id.import_modificationdate}));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent();
		intent.putExtra("url", list.get(position).get("url"));
		setResult(MainActivity.RESULT_IMPORT, intent);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.import1, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.website:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW,
						Uri.parse(MainActivity.WEBSITE_SECURE_URL+"/scripts.php"));
				startActivity(browserIntent);
				return true;
		}
		return false;
	}
}

/*
    Copyright (C) 2012, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.util.Log;

import ss.Parameters;

/**
 * Small java helper for downloading scripts
 *
 * @author Doti
 */
public class DownloadHelper {

	private MainActivity mainActivity;

	public DownloadHelper(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
	}

	/**
	 *
	 * @return filename of created text JSON file
	 */
	public File getJsonFilename() {
		File directory = new File(mainActivity.getTmpFolder().getAbsolutePath());
		directory.mkdirs();
		return new File(directory,"sscripts.json");
	}
	
	public void tryUpdate() {
		try {
			URL url = new URL(Parameters.WEBSITE_URL + "/scripts.php?json=true");
			URLConnection conn = url.openConnection();
			byte[] tmp = new byte[1024];
			InputStream is = conn.getInputStream();
			FileOutputStream os = new FileOutputStream(getJsonFilename());
			int n;
			while ((n = is.read(tmp)) > -1)
				os.write(tmp, 0, n); 
			os.close();
			is.close(); 
		} catch (MalformedURLException e) {
			Log.w("DownloadHelper", e);
		} catch (IOException e) {
			Log.w("DownloadHelper", e);
		}
	}
	

	public void copyFile(String src, String dst) throws IOException {
		if(src.startsWith("https://") || src.startsWith("http://") ||
				src.startsWith("ftp://"))
			copyFile(new URL(src).openStream(), dst);
		else
			copyFile(new FileInputStream(src), dst);
	}
	
	public void copyFile(InputStream src, String dst) throws IOException {
		BufferedInputStream bis = new BufferedInputStream(src);
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(dst));
		byte[] buffer = new byte[1024 * 16];
		int n;
		while ((n = bis.read(buffer)) > -1)
			bos.write(buffer, 0, n);
		bos.close();
		bis.close();
	}
}

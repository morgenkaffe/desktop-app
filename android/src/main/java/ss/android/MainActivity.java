/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.codehaus.groovy.control.MultipleCompilationErrorsException;

import ss.MainWindow;
import ss.PropertiesWorker;
import ss.ScriptContainer;
import ss.TextSource;
import ss.WebCommunicator;

/**
 * Activity showing scripts
 *
 * @author Doti
 */
@SuppressLint("DefaultLocale")
public class MainActivity extends Activity implements MainWindow, TextSource {

	public static final int RESULT_OPTIONS = 1;
	public static final int RESULT_IMPORT = 2;
	public static final int RESULT_OPEN = 3;
	public static final int REQUEST_CAMERA_PICTURE = 4;
	private static final int SHOWBUTTON_WAIT_SECURITY_MARGIN_MS = 10;
	private static final double TOO_THIN_PICTURE_RATIO = 1.4;
	private static final String VALIDITY_DATESTR = "2022-01-01";

	private ScriptContainer sc;
	private PropertiesWorker propertiesWorker;
	private DownloadHelper downloadHelper;
	private GrooidShell grooidShell;
	
	private String imageName = null;
	private byte[] imageBytes = null;
	private String text = "";

	private LinearLayout layout;
	private TextView label;
	private ImageView image;
	private Gauge gauge;
	private Button button;
	private EditText textField;
	private boolean lastBooleanChoice;
	private int lastSelectionChoice;
	private Button button1;
	private Button button2;
	private Spinner comboBox;
	private float speed = 1;
	private Thread thread = null;
	private long buttonStartTimestamp;
	private float buttonResultSeconds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i("MainActivity", "onCreate()");

		if(hasExpired())
			return;

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		propertiesWorker = new PropertiesWorker(this, DEFAULT_PROPERTIES_FILENAME);
		propertiesWorker.updateSystemProperties();
		sc = new ScriptContainer();
		downloadHelper = new DownloadHelper(this);

		grooidShell = new GrooidShell(getTmpFolder(),
				Thread.currentThread().getContextClassLoader());

		setContentView(R.layout.main);

		createFrame();
		if (new File(getDataFolder(), "images").exists()) {
			startIntroScript();
		} else {
			doFirstThings();
		}
	}

	private void doFirstThings() {
		if (!new File(getDataFolder(), "videos").exists()) {
			Log.i("MainActivity",
					"perhapsDoFirstThings() will inflate data.zip into "
							+ getDataFolder().getAbsolutePath());
			new ProgressAsyncTask<Void, Void>(this, this) {
				@Override
				protected Void doInBackground(Void... params) {
					try {
						Log.d("MainActivity", "will copy data.zip");
						File baseFile = new File(getDataFolder(), "data.zip");
						downloadHelper.copyFile(getAssets().open("data.zip"),
								baseFile.getAbsolutePath());
						sc.importScript(MainActivity.this, Uri.fromFile(baseFile).toString());
						Log.d("MainActivity", "inflated data.zip, start intro now");
						runOnUiThread(new Runnable() {
							public void run() {
								startIntroScript();
							}
						});
					} catch (Exception e) {
						Log.w("Mainactivity", "Exception", e);
						showMessage(getString("error", e.toString()), true);
					}
					return null;
				}

			}.execute();
		}
	}

	private void startIntroScript() {
		String startScript = propertiesWorker
				.loadString("application.start_script");
		if (startScript == null)
			startScript = "intro";
		launchSpecialScript(startScript);
	}

	private void createFrame() {
		layout = (LinearLayout) findViewById(R.id.layout);
		image = (ImageView) findViewById(R.id.image);
		label = (TextView) findViewById(R.id.label);
		gauge = (Gauge) findViewById(R.id.gauge);
		button = (Button) findViewById(R.id.button);
		textField = (EditText) findViewById(R.id.textField);
		button1 = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		comboBox = (Spinner) findViewById(R.id.comboBox);

		changeColor(DEFAULT_COLOR);

		image.setVisibility(View.GONE);

		gauge.setBackgroundColor(Color.WHITE);
		gauge.setColor(Color.RED);

		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				synchronized (button) {
					hideUserControls();
					button.notify();
				}
			}
		});

		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				synchronized (button2) {
					lastBooleanChoice = true;
					hideUserControls();
					button2.notify();
				}
			}
		});

		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				synchronized (button2) {
					lastBooleanChoice = false;
					hideUserControls();
					button2.notify();
				}
			}
		});

		comboBox.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
			                           int pos, long id) {
				if (pos > 0) {
					synchronized (comboBox) {
						lastSelectionChoice = pos;
						hideUserControls();
						comboBox.notify();
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		hideUserControls();
	}

	@Override
	public File getDataFolder() {
		return getFilesDir();
	}

	@Override
	public File getTmpFolder() {
		return getCacheDir();
	}

	@Override
	public String readScript(String fileName) {
		Log.d("MainActivity", "readScript : " + fileName);
		sc.currentScriptFileName = fileName;
		try {
			sc.readFromFile();
			if (!sc.forgetScriptAtEnd) {
				String name = new File(getDataFolder(),
						sc.currentScriptFileName).getName();
				name = name.substring(0, name.lastIndexOf("."));
				setTitle("\"" + name + "\"");
			}
			return sc.currentScriptFileName;
		} catch (Exception e) {
			showMessage("Could not read : " + e, true);
			sc.currentScriptFileName = null;
			sc.currentScriptText = "";
			return "";
		}
	}

	@Override
	public void writeScript(String fileName, boolean alsoUpdateInterface) {
		throw new RuntimeException("writeScript not implemented");
	}


    @Override
    public File getFileFromChooser(boolean groovyFilter, File baseFolder, String dialogTitle, Object parent) {
        return new File(new FileChooser(dialogTitle).getFile());
    }

	//public FileChooser getFileChooser(String message) {return new FileChooser(message);}

	private void showOnlineHelp() {
		Intent intent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(WEBSITE_ONLINEHELP_URL));
		startActivity(intent);
	}

	@Override
	public void showAbout() {
		String lastAuthor = sc.savedAuthor;
		if (lastAuthor == null)
			lastAuthor = "/";
		String localModified = "?";
		String yearsString = "2011-";
		Date datelocalModified = getLocalModifiedDate();
		if (datelocalModified != null) {
			DateFormat df = DateFormat.getDateInstance();
			localModified = df.format(datelocalModified);
			yearsString += new SimpleDateFormat("yyyy")
					.format(datelocalModified);
		}
		String onlineModified = "?";
		WebCommunicator webCommunicator = new WebCommunicator();
		Date dateOnlineModified = webCommunicator.getOnlineModifiedDate(
				WebCommunicator.OnlineModifiedFileType.APK);
		if (dateOnlineModified != null) {
			DateFormat df = DateFormat.getDateInstance();
			onlineModified = df.format(dateOnlineModified);
		}
		showMessage(
				getString("about", yearsString, localModified, onlineModified,
						getString("translator"), lastAuthor), false);
	}

	@Override
	public void changeColor(int color) {
		runOnUiThread(new Runnable() {
			private int color;

			public Runnable changeColor(int color) {
				this.color = color;
				return this;
			}

			public void run() {
				//missing alpha
				color = (0xFF << 24) | color;

				float[] hsv = new float[3];
				Color.colorToHSV(color, hsv);
				//in simulator (and elsewhere), hsv is always big (0.9...1)
				Log.d("MainActivity", "Change color : " + color + " (" + hsv[2] + ")");

				((ScrollView) layout.getParent()).setBackgroundColor(color);
				getActionBar().setBackgroundDrawable(new ColorDrawable(
						Color.HSVToColor(
								new float[]{hsv[0], hsv[1], 0.5f * (1 + hsv[2])})));
				if (hsv[2] < .5f)
					label.setTextColor(Color.WHITE);
				else
					label.setTextColor(Color.BLACK);
				int gaugeBackColor = Color.HSVToColor(
						new float[]{hsv[0], hsv[1], 0.33f * (2 + hsv[2])});
				int gaugeFrontColor = Color.HSVToColor(
						new float[]{hsv[0], hsv[1], 0.33f * hsv[2]});
				gauge.setColor(gaugeFrontColor);
				gauge.setBackgroundColor(gaugeBackColor);
				layout.requestLayout();
			}
		}.changeColor(color));
	}

	@Override
	public void setTitle(String title) {
		runOnUiThread(new Runnable() {
			private String title;

			public Runnable setTitle(String title) {
				this.title = title;
				return this;
			}

			public void run() {
				title = title.trim();
				if (title.equals(""))
					title = getString("title");
				MainActivity.super.setTitle(title);
			}
		}.setTitle(title));
	}

	@Override
	public void updateTextAndImage() {
		runOnUiThread(new Runnable() {
			public void run() {
				label.setText(
						Html.fromHtml("<html>" + text.trim().replace("\n", "<br />")
								+ "</html>"));
				if (imageName != null && imageName != "") {
					image.setVisibility(View.GONE);
					try {
                        Bitmap bmp;
					    if(imageName.equals("data")) {
							 bmp = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
					    } else {
							BitmapFactory.Options options = new BitmapFactory.Options();
							options.inJustDecodeBounds = false;
							options.inPreferredConfig = Bitmap.Config.RGB_565;
							bmp = BitmapFactory.decodeFile(imageName, options);
						}
						image.setImageBitmap(bmp);
						
						/* not needed, seems weaker
						Display display = getWindowManager().getDefaultDisplay();
						Point size = new Point();
						display.getSize(size);
						int width = size.x;
						*/
						int width = layout.getWidth();
						LinearLayout.LayoutParams lp = null;
						if (bmp.getHeight() > bmp.getWidth()) {
							if (bmp.getHeight() > bmp.getWidth() * TOO_THIN_PICTURE_RATIO)
								width *= bmp.getWidth() * TOO_THIN_PICTURE_RATIO / bmp.getHeight();
							lp = new LinearLayout.LayoutParams(
									width, width);
							lp.gravity = Gravity.CENTER_HORIZONTAL;
						} else {
							lp = new LinearLayout.LayoutParams(
									width, width * bmp.getHeight() / bmp.getWidth());
						}
						image.setLayoutParams(lp);
						image.setVisibility(View.VISIBLE);
						image.requestLayout();
					} catch (java.lang.OutOfMemoryError ex) {
						showMessage(getString("Error", ex.toString() + " (" + imageName + ")"), true);
					} catch (Exception ex) {
					}
				} else {
					image.setVisibility(View.GONE);
					image.requestLayout();
				}
			}
		});
	}

	@Override
	public void setWaiting(boolean waiting) {
		// perhaps an actionbar icon
	}

	@Override
	public void showMessage(String text, boolean error) {
		if (error)
			Log.w("MainActivity", "showMessage(" + text.replace("\n", " ; ")
					+ "," + error + ")");
		else
			Log.i("MainActivity", "showMessage(" + text.replace("\n", " ; ")
					+ "," + error + ")");
		String errorStr = getString("title");
		if (error)
			errorStr = getString("error", "");
		AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setMessage(text);
		b.setTitle(errorStr);
		b.setPositiveButton("ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				synchronized (button2) {
					button2.notify();
				}
			}
		});
		if (!isUiThread()) {
			Log.i("showMessage", "!isUiThread");
			runOnUiThread(new Runnable() {
				private AlertDialog.Builder b;

				public Runnable init(AlertDialog.Builder b) {
					this.b = b;
					return this;
				}

				public void run() {
					b.create().show();
				}
			}.init(b));
			try {
				synchronized (button2) {
					button2.wait();
				}
			} catch (InterruptedException ex) {
				//impossible
			}
		} else
			b.create().show();
	}

	@Override
	public String getInputString(String string, String defaultString) throws InterruptedException {
		throw new RuntimeException("getInputString() not implemented");
	}

	@Override
	public void setPlayingSound(boolean playingSound) {
		// perhaps an actionbar icon
	}

	@Override
	public PropertiesWorker getPropertiesWorker() {
		return propertiesWorker;
	}

	@Override
	public ScriptContainer getScriptContainer() {
		return sc;
	}

	@Override
	public void showFromScript(String s) {
		if (s == null)
			text = "";
		else
			text = s;
		updateTextAndImage();
	}

	@Override
	public float showPopupFromScript(String s) {
		buttonStartTimestamp = System.currentTimeMillis();
		showMessage(s, false);
		buttonResultSeconds = (System.currentTimeMillis() - buttonStartTimestamp) / 1000.0f;
		return speed * buttonResultSeconds;
	}

	@Override
	public double showButtonFromScript(String s, double duration) throws InterruptedException {
		if (s == null) {
			s = "";
		}
		runOnUiThread(new Runnable() {
			private String s;

			public Runnable init(String s) {
				this.s = s;
				return this;
			}

			public void run() {
				buttonStartTimestamp = System.currentTimeMillis();
				button.setText(s.trim());
				button.setVisibility(View.VISIBLE);
			}
		}.init(s));
		synchronized (button) {
			button.wait((long) (duration * 1000 / speed)
					+ SHOWBUTTON_WAIT_SECURITY_MARGIN_MS);
		}
		runOnUiThread(new Runnable() {
			public void run() {
				button.setVisibility(View.GONE);
			}
		});
		buttonResultSeconds = speed
				* (System.currentTimeMillis() - buttonStartTimestamp)
				/ 1000.0f;
		return Math.min(duration, buttonResultSeconds);
	}

	@Override
	public String getStringFromScript(String text, String defaultValue) throws InterruptedException {
		if (text != null) {
			showFromScript(text);
		}
		runOnUiThread(new Runnable() {
			private String defaultValue;

			public Runnable init(String defaultValue) {
				this.defaultValue = defaultValue;
				return this;
			}

			public void run() {
				textField.setText(defaultValue);
				textField.setVisibility(View.VISIBLE);
				button2.setText(android.R.string.ok);
				button2.setVisibility(View.VISIBLE);
			}
		}.init(defaultValue));
		synchronized (button2) {
			button2.wait();
		}

		return textField.getText().toString();
	}

	@Override
	public boolean getBooleanFromScript(String text, String yesMessage,
	                                    String noMessage) throws InterruptedException {
		if (text != null) {
			showFromScript(text);
		}
		runOnUiThread(new Runnable() {
			private String yesMessage;
			private String noMessage;

			public Runnable init(String yesMessage, String noMessage) {
				this.yesMessage = yesMessage;
				this.noMessage = noMessage;
				return this;
			}

			public void run() {
				button1.setText(yesMessage);
				button1.setVisibility(View.VISIBLE);
				button2.setText(noMessage);
				button2.setVisibility(View.VISIBLE);
			}
		}.init(yesMessage, noMessage));
		boolean res = true;
		synchronized (button2) {
			button2.wait();
			res = lastBooleanChoice;
		}

		return res;
	}

	@Override
	public int getSelectedValueFromScript(String text, List<String> values) throws InterruptedException {
		if (text != null) {
			showFromScript(text);
		}
		runOnUiThread(new Runnable() {
			private List<String> values;

			public Runnable init(List<String> values) {
				this.values = values;
				return this;
			}

			public void run() {
				ArrayList<String> allValues = new ArrayList<String>();
				allValues.add("?");
				for (String value : values)
					allValues.add(value);
				Log.d("MainActivity", "getSelectedValueFromScript() : " +
						allValues.size() + " (-1) values");
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						MainActivity.this,
						android.R.layout.simple_spinner_dropdown_item, allValues);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				comboBox.setAdapter(adapter);
				comboBox.setVisibility(View.VISIBLE);
			}
		}.init(values));
		synchronized (comboBox) {
			comboBox.wait();
		}
		return comboBox.getSelectedItemPosition() - 1;
	}

	@Override
	public void waitFromScript(double duration) throws InterruptedException {
		// 10 waits (and not 1) : in case of speed change
		for (int i = 0; i < 10; i++)
			Thread.sleep((int) (duration * 100 / speed));
	}

	@Override
	public void waitWithGaugeFromScript(double duration) throws InterruptedException {
		runOnUiThread(new Runnable() {
			public void run() {
				gauge.setVisibility(View.VISIBLE);
			}
		});
		int msTot = (int) (duration * 1000 / speed);
		for (int i = 0; i < msTot; i += 40) {
			gauge.setPrc(100 * (double) i / msTot);
			gauge.postInvalidate();
			Thread.sleep(40);
			// in case of speed change
			msTot = (int) (duration * 1000 / speed);
		}
		runOnUiThread(new Runnable() {
			public void run() {
				gauge.setVisibility(View.GONE);
			}
		});
	}

	@Override
	public void setImageFromScript(String path) {
		this.imageName = path;
		this.imageBytes = null;
		updateTextAndImage();
	}

	/**
	* [...] from an array of bytes which were read from an image file containing
	* 	a supported image format, such as GIF, JPEG, or (as of 1.3) PNG [...]
	*/
	@Override
	public void setImageFromScript(byte[] bytes) {
		this.imageName = "data";
		this.imageBytes = bytes;
		updateTextAndImage();
	}

	private boolean launchSpecialScript(String name) {
		sc.forgetScriptAtEnd = true;
		return launchScript(name);
	}

	private boolean launchScript(String name) {
		try {
			String nameWithoutExt = name;
			if (nameWithoutExt.toLowerCase().endsWith(".groovy"))
				nameWithoutExt = nameWithoutExt.substring(0, nameWithoutExt
						.toLowerCase().lastIndexOf(".groovy"));
			String[] wantedFileNames = {
					new File(getDataFolder(), MainWindow.SCRIPT_FOLDER
							+ nameWithoutExt + "_"
							+ Locale.getDefault().getLanguage() + "_"
							+ Locale.getDefault().getCountry() + ".groovy")
							.getAbsolutePath(),
					new File(getDataFolder(), MainWindow.SCRIPT_FOLDER
							+ nameWithoutExt + "_"
							+ Locale.getDefault().getLanguage() + ".groovy")
							.getAbsolutePath(),
					new File(getDataFolder(), MainWindow.SCRIPT_FOLDER
							+ nameWithoutExt + ".groovy").getAbsolutePath()};
			String currentScript = "";
			for (String wantedFileName : wantedFileNames)
				if (new File(wantedFileName).exists()) {
					currentScript = readScript(wantedFileName);
					break;
				}
			if (currentScript == null || currentScript.equals("")) {
				Log.i("MainActivity", "launchScript(" + name
						+ ") : finally not found");
				return false;
			} else {
				Log.d("MainActivity", "launchScript(" + name
						+ ") : is current, will run");
			}
			run();
		} catch (Exception e) {
			Log.w("Mainactivity", "Exception", e);
			showMessage(getString("error", e), true);
		}
		return true;
	}

	@Override
	public void run() {
		Log.d("MainActivity", "run()");
		stopScriptThreads();
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			}
		}); 
		thread = new Thread() {
			public void run() {
				hideUserControls();
				String nextScript = null;
				try {
					/*String fullScript = "class FullScript_Stub extends ss.android.Script {\n"+
							"public FullScript_Stub(ss.MainWindow mw, ss.TextSource ts){ super(mw, ts); }\n"+
							"public String run(){ android.util.Log.i(\"MainActivity\",\"Ok from FullScriptStub\"); return null; }\n"+
							"}";*/
					String fullScript = sc.getScriptClassText(false);

					if (fullScript == null) {
						showMessage(getString("error", "No script"), true);
						return;
					}
					Log.v("MainActivity", "Full script : \n"+fullScript);
					Map<String, Class<?>> classes = grooidShell.evaluate(fullScript);
					Log.d("MainActivity", "Found " + classes.size() + " classes.");
					for (String className : classes.keySet()) {
						if (className.startsWith("ss.FullScript_") && !className.contains("$")) {
							Log.d("MainActivity", "run " + className);
							sc.currentScript = (Script) classes
									.get(className)
									.getConstructor(MainWindow.class,
											TextSource.class)
									.newInstance(MainActivity.this,
											MainActivity.this);
							//set working directory ; works at last with 4.0.3 and new File(...)
							System.setProperty("user.dir", getDataFolder().getAbsolutePath());
							try {
								nextScript = (String) sc.currentScript.run();
							} catch (InterruptedException ex2) {
								Log.i("MainActivity", className + " interrupted");
							}
							Log.d("MainActivity", "End " + className + ", next:" + nextScript);
						}
					}
				} catch (MultipleCompilationErrorsException e) {
					Log.i("MainActivity", "Error MCEE on " + sc.currentScriptFileName, e);
					showMessage(getString("error", "[Invalid script] " + e.getMessage()), true);
				} catch (Throwable e) {
					Log.e("MainActivity", "Error", e);
					showMessage(getString("error", e.getMessage()), true);
				}
				boolean ended;
				if (nextScript != null)
					ended = !launchScript(nextScript);
				else
					ended = true;
				if (ended) {
					changeColor(DEFAULT_COLOR);
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
						}
					});
					if (sc.forgetScriptAtEnd) {
						sc.currentScriptFileName = null;
						sc.currentScriptText = "";
						sc.savedAuthor = null;
						sc.forgetScriptAtEnd = false;
					}
					setTitle("");
				}
			}
		};
		thread.start();
	}

	@Override
	public void stopScriptThreads() {
		if (thread != null && thread.isAlive()
				&& thread != Thread.currentThread()) {
			thread.interrupt();
			try {
				thread.join();
			} catch (InterruptedException ex) {
			}
		}
	}

	@Override
	public String getString(String s, Object... args) {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("texts");
			return String.format(bundle.getString(s), args);
		} catch (Exception e) {
			return String.format(s, args);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		updateMenuString(menu);
		return true;
	}

	private void updateMenuString(Menu menu) {
		for (int i = 0; i < menu.size(); i++) {
			MenuItem mi = menu.getItem(i);
			if (!"".equals(mi.getTitle()))
				mi.setTitle(getString((String) mi.getTitle()));
			if (mi.getSubMenu() != null)
				updateMenuString(mi.getSubMenu());
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.speed_slow).setChecked(speed == .5f);
		menu.findItem(R.id.speed_normal).setChecked(speed == 1);
		menu.findItem(R.id.speed_fast).setChecked(speed == 2);
		menu.findItem(R.id.speed_faster).setChecked(speed == 4);
		// TODO reset (?)
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
			case R.id.importScript:
				new ProgressAsyncTask<Void, File>(this, this) {

					@Override
					protected File doInBackground(Void... params) {
						downloadHelper.tryUpdate();
						File jsonFilename = downloadHelper.getJsonFilename();
						return jsonFilename;
					}

					@Override
					protected void onPostExecute(File jsonFilename) {
						super.onPostExecute(jsonFilename);
						if (jsonFilename == null) {
							showMessage(
									getString("error", "Could not get script list"),
									true);
						} else {
							Intent intent = new Intent(MainActivity.this,
									ImportActivity.class);
							intent.putExtra("jsonFilename", jsonFilename.getAbsolutePath());
							startActivityForResult(intent, 0);
						}
					}

				}.execute();
				break;
			case R.id.open:
				Intent intent = new Intent(this, OpenActivity.class);
				intent.putExtra("scriptList", getScriptList());
				startActivityForResult(intent, 0);
				break;
			case R.id.toys:
				launchSpecialScript("toys");
				break;
			case R.id.womensclothes:
				launchSpecialScript("womensclothes");
				break;
			case R.id.mensclothes:
				launchSpecialScript("mensclothes");
				break;
			case R.id.speed_slow:
				speed = .5f;
				break;
			case R.id.speed_normal:
				speed = 1;
				break;
			case R.id.speed_fast:
				speed = 2;
				break;
			case R.id.speed_faster:
				speed = 4;
				break;
			case R.id.onlinehelp:
				showOnlineHelp();
				break;
			case R.id.about:
				showAbout();
				break;
			default:
				return false;
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("MainActivity", "onActivityResult(" + requestCode + ","
				+ resultCode + ")");
		switch (resultCode) { // TODO enum
			case RESULT_OPTIONS:
				if (sc != null && sc.currentScript != null)
					((Script) sc.currentScript).onActivityResult(data);
				break;
			case RESULT_IMPORT:
				String scriptUrl = data.getStringExtra("url");
				if (scriptUrl != null) {
					new ProgressAsyncTask<String, Void>(this, this) {
						@Override
						protected Void doInBackground(String... params) {
							try {
							    Log.i("MainActivity",
                                    "onActivityResult() : will import "
                                            + params[0]);
								Object[] importRes = sc.importScript(MainActivity.this, params[0]);
								int nbScripts = (Integer) importRes[0];
								String firstName = "";
								if (nbScripts > 0)
									firstName = ", " + (String) importRes[1];
								showMessage(getString("import.ok", nbScripts, firstName),
										false);
								Log.i("MainActivity",
										"onActivityResult() : Imported "
												+ params[0]);
							} catch (Exception e) {
								Log.w("Mainactivity", "Exception", e);
								showMessage(getString("error", e+" (quotas ?)"), true);
							}
							return null;
						}
					}.execute(scriptUrl);
				}
				break;
			case RESULT_OPEN:
				String name = data.getStringExtra("name");
				if (name != null)
					launchScript(name);
				break;
			case RESULT_OK:
			case RESULT_CANCELED:
				if (requestCode == REQUEST_CAMERA_PICTURE && sc != null &&
						sc.currentScript != null)
					((Script) sc.currentScript).onActivityResult(data);
				break;
		}
	}

	private String[] getScriptList() {
		return getScriptList(SCRIPT_FOLDER);
	}

	private String[] getScriptList(String folder) {
		List<String> res = new ArrayList<String>();
		for (File f : new File(getDataFolder(), folder).listFiles()) {
			if (f.getName().endsWith(".groovy"))
				res.add(f.getName());
			if (f.isDirectory()) {
				for(String sf : getScriptList(folder + f.getName()+"/"))
					res.add(f.getName()+"/"+sf);
			}
		}
		Collections.sort(res, String.CASE_INSENSITIVE_ORDER);
		return res.toArray(new String[]{});
	}
	/*
		private int importScript(String url) throws IOException {
			String localUrl = url;
			coulnt find why do this ? old sc version ?
			if (!url.startsWith("file://")) {
				File newZipFile = new File(getDataFolder(), url.hashCode() + ".zip");
				localUrl = newZipFile.getAbsolutePath();
				downloadHelper.copyFile(url, localUrl);
			}
			int res = (Integer)sc.importScript(this, localUrl)[0];
		Log.i("MainActivity", "importScript(" + url + " / " + localUrl + ") : "
				+ res);
		return res;
	}*/

	private boolean isUiThread() {
		return Thread.currentThread() == getMainLooper().getThread();
	}

	private void hideUserControls() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				gauge.setVisibility(View.GONE);
				button.setVisibility(View.GONE);
				textField.setVisibility(View.GONE);
				button1.setVisibility(View.GONE);
				button2.setVisibility(View.GONE);
				comboBox.setVisibility(View.GONE);
			}
		});
	}

	private Date getLocalModifiedDate() {
		Date res = null;
		try {
			PackageInfo packageInfo = getPackageManager()
					.getPackageInfo(getPackageName(), 0);
			res = new Date(packageInfo.lastUpdateTime);
		} catch (Exception e) {
		}
		return res;
	}

	private boolean hasExpired() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date validityDate = df.parse(VALIDITY_DATESTR);
			if(new Date().getTime()>validityDate.getTime()) {
				//finish => can not use showMessage()
				AlertDialog.Builder b = new AlertDialog.Builder(this);
				b.setMessage("Release expired, please download a new release of SexScripts on " + WEBSITE_URL);
				b.setTitle(getString("title"));
				b.setPositiveButton("ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						MainActivity.this.finish();
					}
				});
				b.show();
				return true;
			}
		} catch(ParseException ex) {}
		return false;
	}

	@Override
	public void dispose() {
		stopScriptThreads();
	}

	public class FileChooser {
		private String message;
		private File file;
		private String[] files;

		public FileChooser(String message) {
			this(message, Environment.
					getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));
		}

		public FileChooser(String message, File dir) {
			this.message = message;
			file = dir;
		}

		public String getFile() {
			while (file != null && file.isDirectory()) {
				runOnUiThread(new Runnable() {
					public void run() {
						AlertDialog ad = null;
						try {
							AlertDialog.Builder builder = new AlertDialog.Builder(
									MainActivity.this);
							builder.setTitle(message + " ("
									+ file.getAbsolutePath() + ")");
							String[] onlyFiles = file.list();
							if(onlyFiles==null) //no SDCard ?
								files = new String[1];
							else
								files = new String[onlyFiles.length + 1];
							files[0] = "..";
							int i = 0;
							for (String f : onlyFiles) {
								files[i + 1] = f;
								i++;
							}
							builder.setItems(files,
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
										                    int which) {
											file = new File(file, files[which]);
											try {
												file = new File(file.getCanonicalPath());
											} catch (IOException ex) {
											}
											Log.d("FileChooser", "Chosen file : " +
													file.getAbsolutePath() +
													" (" + file.isDirectory() + ")");
											dialog.dismiss();
											synchronized (FileChooser.this) {
												FileChooser.this.notify();
											}
										}
									});
							builder.setPositiveButton(getString("android.cancel"),
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int i) {
											file = null;
											Log.d("FileChooser", "Cancelled");
											dialog.dismiss();
											synchronized (FileChooser.this) {
												FileChooser.this.notify();
											}
										}
									});
							builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
								@Override
								public void onCancel(DialogInterface dialogInterface) {
									file = null;
									Log.d("FileChooser", "Cancelled (back btn)");
									synchronized (FileChooser.this) {
										FileChooser.this.notify();
									}
								}
							});
							ad = builder.show();
						} catch(Exception ex) {
							if(ad!=null)
								ad.cancel();
							Log.d("FileChooser", "Exception somewhere", ex);
							synchronized (FileChooser.this) {
								FileChooser.this.notify();
							}
						}
					}
				});
				try {
					synchronized (FileChooser.this) {
						FileChooser.this.wait();
					}
				} catch (InterruptedException e) {
				}
			}
			if (file == null)
				return null;
			else
				return file.getAbsolutePath();
		}
	}
}

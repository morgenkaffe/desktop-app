/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;

import ss.IScript;
import ss.MainWindow;
import ss.Parameters;
import ss.PropertiesWorker;
import ss.TextSource;
import ss.WebCommunicator;

/**
 * <p>
 * This class is extended by scripts (they are put inside run()).
 * </p>
 * 
 * @author Doti
 */
public class Script implements IScript {

	private static final String[] IMAGE_EXTENSIONS = new String[] { "gif",
			"png", "bmp", "jpeg", "jpg" };
	private static final String RECEIVED_IMAGE_FOLDER = "received/";

	private MainActivity main;
	private Set<MediaPlayer> mediaPlayers;
	private Intent lastResult;
	private WebCommunicator webCommunicator;

	public Script(MainActivity main, TextSource textSourceUnu) {
		this.main = main;
		mediaPlayers = new HashSet<MediaPlayer>();
		webCommunicator = new WebCommunicator();
		if (main != null)
			main.getScriptContainer().clearUsedFiles();
	}

	public void finalize() {
		stopSoundThreads();
	}

	@Override
	public void stopSoundThreads() {
		for(MediaPlayer mp : mediaPlayers) {
			if (mp.isPlaying())
				mp.stop();
			mp.release();
		}
		main.setPlayingSound(false);
	}

	public void setInfos(int version, String title, String summary,
			String author, String status, int color, String lang,
			List<String> tags) throws InterruptedException {
		main.changeColor(color);
		main.setTitle("\"" + title + "\"");
		main.getScriptContainer().update(version, title, summary, author,
				status, color, lang, tags);
		if (version > MainWindow.API_VERSION) {
			main.showMessage(main.getString("error.version", version,
					MainWindow.API_VERSION), true);
		}
	}

	@Override
	public void show(Object s) {
		if (s == null)
			main.showFromScript(null);
		else
			main.showFromScript(String.valueOf(s));
	}

	@Override
	public double showButton(Object s, double duration) throws InterruptedException {
		return main.showButtonFromScript(String.valueOf(s), duration);
	}

	public double showButton(Object s) throws InterruptedException {
		return showButton(s, 30 * 24 * 3600);
	}

	public float showPopup(Object s) {
		return main.showPopupFromScript(String.valueOf(s));
	}

	public String getDataFolder() {
	    String s = main.getDataFolder().getAbsolutePath()+"/";
	    if(s.endsWith("./"))
	    	s = s.substring(0, s.length()-2);
	    return s;
	}

	public String getString(Object text, Object defaultValue) throws InterruptedException {
		String realText = null;
		if (text != null)
			realText = String.valueOf(text);
		return main.getStringFromScript(realText,
				String.valueOf(defaultValue));
	}

	public Integer getInteger(Object text, Integer defaultValue) throws InterruptedException {
		Integer res = null;
		do {
			String textRes = getString(text, String.valueOf(defaultValue));
			try {
				res = new Integer(textRes);
			} catch (Exception e) {
			}
		} while (res == null);
		return res;
	}

	public Float getFloat(Object text, Float defaultValue) throws InterruptedException {
		Float res = null;
		do {
			String textRes = getString(text, String.valueOf(defaultValue));
			try {
				res = new Float(textRes);
			} catch (Exception e) {
			}
		} while (res == null);
		return res;
	}

	public boolean getBoolean(Object text, Object yesMessage, Object noMessage) throws InterruptedException {
		String realText = null;
		if (text != null)
			realText = String.valueOf(text);
		return main.getBooleanFromScript(realText,
				String.valueOf(yesMessage), String.valueOf(noMessage));
	}

	public boolean getBoolean(Object text) throws InterruptedException {
		return getBoolean(text,
				//android.R.string.yes => "ok" !
				main.getString("android.yes"),
				main.getString("android.no"));
	}

	public int getSelectedValue(Object text, List<String> values) throws InterruptedException {
		String realText = null;
		if (text != null)
			realText = String.valueOf(text);
		return main.getSelectedValueFromScript(realText, values);
	}

	public List<Boolean> getBooleans(Object title, List<String> values,
			List<Boolean> defaultValues) throws InterruptedException {
		Intent intent = new Intent(main, OptionsActivity.class);
		intent.putExtra("title", String.valueOf(title));
		intent.putExtra("values", values.toArray(new String[0]));
		//put a Object[] anyway, thanks google
		//i.putExtra("defaultValues", (Boolean[])defaultValues.toArray(new Boolean[0]));
		String [] defaultValuesTmp = new String[defaultValues.size()];
		for(int i = 0;i<defaultValues.size();i++)
			defaultValuesTmp[i] = String.valueOf(defaultValues.get(i));;
		intent.putExtra("defaultValues", defaultValuesTmp);
		main.startActivityForResult(intent, 0);
		synchronized (this) {
			wait();
		}
		boolean[] result = lastResult.getExtras().getBooleanArray("result");
		ArrayList<Boolean> r2 = new ArrayList<Boolean>();
		for (boolean b : result)
			r2.add(b);
		return r2;
	}

	@Override
	public String getImage(Object message) throws InterruptedException {
		Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if(i.resolveActivity(main.getPackageManager())!=null) {
			String filename = String.valueOf((int) (Math.random() * Integer.MAX_VALUE));
			File storageDir = Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_PICTURES);
			try {
				File image = File.createTempFile(filename, ".jpg", storageDir);
				i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
				//TODO this should be done in main
				main.startActivityForResult(i, MainActivity.REQUEST_CAMERA_PICTURE);
				synchronized (this) {
					Log.d("Script", "Start waiting for get image");
					wait();
					Log.d("Script", "End waiting for get image");
				}
				if(image.exists() && image.length()>1000)
					return image.getCanonicalPath();
				else
					return getFile(message);
			} catch (IOException ex) {
				Log.i("Script", "IOException for get image", ex);
			}
		} else {
			Log.i("Script", "No activity for image capture");
		}
		return getFile(message);
	}

	@Override
	public int getRandom(Integer max) {
		if (max == null)
			max = 100;
		return (int) (Math.random() * max);
	}

	@Override
	public void replacedByWait(double duration) throws InterruptedException {
		main.waitFromScript(duration);
	}

	@Override
	public void waitWithGauge(double duration) throws InterruptedException {
		main.waitWithGaugeFromScript(duration);
	}

	public int getTime() {
		return (int) (Math.round((System.currentTimeMillis() + TimeZone
				.getDefault().getOffset(System.currentTimeMillis())) / 1000.0));
	}

	public String getFile(Object message) throws InterruptedException {
        File srcFile = main.getFileFromChooser(false, null, String.valueOf(message), null);
        if (srcFile != null) {
            try {
                return srcFile.getCanonicalPath();
            } catch (IOException e) {
                return null;
            }
        } else {
            return null;
        }
	}

	public String loadString(String key) {
		return main.getPropertiesWorker().loadString(key);
	}

    public Integer loadInteger(String key) {
		return main.getPropertiesWorker().loadInteger(key);
	}

    public Float loadFloat(String key) {
		return main.getPropertiesWorker().loadFloat(key);
	}

	public Boolean loadBoolean(String key) {
		return main.getPropertiesWorker().loadBoolean(key);
	}

	public String loadFirstTrue(String... keys) {
		return main.getPropertiesWorker().loadFirstTrue(keys);
	}

    @Override
    public Map<String, ?> loadMap(String key) {
        return (Map<String, ?>)load(key);
    }

    @Override
    public Object load(String key) {
        return main.getPropertiesWorker().load(key);
    }

	public void save(String key, Object v) {
		main.getPropertiesWorker().save(key, v);
	}

	public void setImage(String fileName) {
		try {
			if (fileName == null || fileName.trim().equals("")) {
				main.setImageFromScript("");
			} else {
				File f;
				if (fileName.startsWith("/") || fileName.contains(":"))
					f = new File(fileName);
				else
					f = new File(main.getDataFolder(),MainWindow.VIDEO_FOLDER + fileName);
				if (!f.exists()) {
					f = new File(main.getDataFolder(),MainWindow.IMAGE_FOLDER + fileName);
				}
				if (!f.exists()) {
					showPopup(main.getString("missing", f.getPath()));
				} else {
					String extension = f.getPath().substring(
							f.getPath().lastIndexOf(".") + 1);
					if (Arrays.asList(IMAGE_EXTENSIONS).contains(extension)) {
						main.setImageFromScript(f.getCanonicalPath());
					} else if (Arrays.asList(
							new String[] { "mpg", "mpeg", "avi", "asf", "h264",
									"rv", "mov", "wmf", "3gp" }).contains(
							extension)) {
						useFile(f.getCanonicalPath());
					}
				}
			}
		} catch (Exception e) {
			showPopup(main.getString("error", e.toString()));
		}
	}

	@Override
	public void setImage(byte[] bytes, int useless) {
//TODO
		main.setImageFromScript(bytes);
	}

	public void useFile(String fileName) {
		Log.d("Script", "useFile("+fileName+")");
		String mimeType = "?";
		try {
			Intent i = new Intent();
			File file = getRealResourceFile(fileName, "");
			MimeTypeMap myMime = MimeTypeMap.getSingleton();
			Intent newIntent = new Intent(Intent.ACTION_VIEW);
			mimeType = myMime.getMimeTypeFromExtension(fileName.substring(fileName.lastIndexOf(".")+1));
			i.setDataAndType(Uri.fromFile(file), mimeType);
			i.setAction(Intent.ACTION_VIEW);
			main.startActivity(i);
		} catch (Exception e) {
			showPopup(main.getString("error", fileName + " \u2192? ("+mimeType+"?"));
		}
	}

	public void useUrl(String url) {
		try {
			Intent i = new Intent();
			i.setData(Uri.parse(url));
			i.setAction(Intent.ACTION_VIEW);
			main.startActivity(i);
		} catch (Exception e) {
			showPopup(main.getString("error", url + " " + e.toString()));
		}
	}

	public void useEmailAddress(String address) {
		try {
			Intent i = new Intent();
			i.setData(Uri.parse(address));
			i.setAction(Intent.ACTION_SENDTO);
			main.startActivity(i);
		} catch (ActivityNotFoundException e) {
			showPopup("Send manually an email to "+address);
		} catch (Exception e) {
			showPopup(main.getString("error", address + " " + e.toString()));
		}
	}

	public void playSound(String fileName) throws InterruptedException {

		File f = getRealResourceFile(fileName, MainWindow.SOUND_FOLDER);
		try {
			MediaPlayer mediaPlayer = startSound(fileName, 1, true);
			synchronized (this) {
				Log.d("Script", "Start waiting for sound");
				wait();
				Log.d("Script", "End waiting for sound");
			}
		} catch (Exception e) {
			main.setPlayingSound(false);
		}
	}


    public void playBackgroundSound(String fileName) {
        playBackgroundSound(fileName, 1);
    }

	public void playBackgroundSound(String fileName, int times) {
		try {
			startSound(fileName, times, false);
		} catch (Exception e) {
		}
	}

	private MediaPlayer startSound(String fileName, int times, final boolean notify) throws IOException {
		File f = getRealResourceFile(fileName, MainWindow.SOUND_FOLDER);
		MediaPlayer mediaPlayer = new MediaPlayer();
		// notice : WAV possibly fails on [1.4,4.1[
		mediaPlayer.setDataSource(f.getAbsolutePath());
		mediaPlayer.prepare();
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			private String fileName;
			private int times;

            public OnCompletionListener init(String fileName, int times) {
                this.times = times;
                this.fileName = fileName;
                return this;
            }

			@Override
			public void onCompletion(MediaPlayer mediaPlayer) {
                times--;
                if(times > 0) {
                    mediaPlayer.reset();
					try {
                        //only start() : does not work
    					mediaPlayer.setDataSource(fileName);
	    				mediaPlayer.prepare();
                        mediaPlayer.start();
                    } catch(IOException ex) {
                    	Log.w("Script", "startSound() exception", ex);
                    }
                } else {
                    mediaPlayer.release();
                    mediaPlayers.remove(mediaPlayer);
                    main.setPlayingSound(mediaPlayers.size() > 0);
                    if (notify) {
                        synchronized (Script.this) {
                            Script.this.notify();
                        }
                    }
                }
			}
		}.init(f.getAbsolutePath(), times));
		main.setPlayingSound(true);
		mediaPlayer.start();
		return mediaPlayer;
	}

	public void openCdTrays() {
		Vibrator v = (Vibrator) main.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(1000);
	}

	@Override
	public boolean isConnected() throws InterruptedException {
		Map<String, String> data = new HashMap<>();
		data.put("action", "test");
		try {
			String s = webCommunicator.send(data, null).trim();
			return s.equals("ok");
		} catch (IOException ex) {
			return false;
		}
	}

	@Override
	public void send(String key, Object value) throws InterruptedException {
		if(key.length()> Parameters.SSCA_KEY_LIMIT_LENGTH) {
			throw new InvalidParameterException("Key "+key.substring(0,10)+"... too long");
		}

        Map<String, String> props;

        String valueStr;
        if(value!=null) {
            valueStr = String.valueOf(value);
            if(valueStr.length()> Parameters.SSCA_VALUE_LIMIT_LENGTH) {
                throw new InvalidParameterException("Value "+valueStr.substring(0,10)+"... too long");
            }
            props = PropertiesWorker.getPropertiesListFromObjectRec(key,  value);
        } else {
            props = new HashMap<String, String>();
        }
        Map<String, String> data = new HashMap<String, String>();
        data.put("action", "ssca_send_mlt");
        data.put("basekey", key);
        for(String detailKey:props.keySet()) {
            data.put("data["+detailKey+"]", props.get(detailKey));
        }

        try {
            System.out.println(webCommunicator.send(data, null));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

	}

    @Override
    public Boolean receiveBoolean(String key) {
        try {
            return (Boolean)receive(key);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Float receiveFloat(String key) {
        try {
            return (Float)receive(key);
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public Integer receiveInteger(String key) {
        try {
            return (Integer)receive(key);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
	public String receiveString(String key) throws InterruptedException {
        Object o = receive(key);
        if(o==null)
            return null;
        else
            return String.valueOf(o);
	}


    @Override
    public Map<String, ?> receiveMap(String key) throws InterruptedException {
        return (Map<String, ?>)receive(key);
    }

    @Override
    public Object receive(String key) throws InterruptedException {
        Map<String, String> data = new HashMap<String, String>();
        data.put("action", "ssca_receive_mlt");
        data.put("key", key);
        try {
            String s = webCommunicator.send(data, null);
            Properties props = new Properties();
            props.load(new StringReader(s));
            if(props.containsKey("error"))
                throw new InvalidParameterException("Key "+key+" : "+props.getProperty("error"));
            Log.d("receive",key + " : \""+s+"\"");
            return PropertiesWorker.getObjectFromPropertiesListRec(key, props);
        } catch (IOException ex) {
            Log.i("receive",key + " : exception "+ex);
            return null;
        }
    }

    @Override
	public String sendImage(String filename) {
		if (!new File(filename).exists())
			return null;
		String ext = filename.substring(filename.lastIndexOf(".") + 1);
		if (!Arrays.asList(IMAGE_EXTENSIONS).contains(ext.toLowerCase()))
			return null;
		Map<String, String> data = new HashMap<String, String>();
		data.put("action", "ssca_sendimage");
		List<String> files = new ArrayList<String>();
		files.add(filename);
		try {
			// receive "Ok codename1.png,codename2.jpeg,..."
			String res = webCommunicator.send(data, files);
			if (res.startsWith("Ok"))
				return res.substring(res.indexOf(' ') + 1).split(",")[0].trim();
			else
				return null;
		} catch (IOException ex) {
			System.out.println(ex);
			return null;
		}
	}

	@Override
	public String receiveImage(String code) {
		Log.d("Script", "receiveImage : "+code);
		String ext = code.substring(code.lastIndexOf(".") + 1);
		if (!Arrays.asList(IMAGE_EXTENSIONS).contains(ext.toLowerCase()))
			return null;
		File imageFolder = new File(main.getDataFolder(),MainWindow.IMAGE_FOLDER + RECEIVED_IMAGE_FOLDER);
		File file = new File(imageFolder, code);
		String result = RECEIVED_IMAGE_FOLDER+code;
		if (file.exists())
			return result;
		try {
			Map<String, String> data = new HashMap<String, String>();
			data.put("action", "ssca_receiveimage");
			data.put("code", code);
			imageFolder.mkdirs();
			if (webCommunicator.sendAndSaveToFile(data, null, file.getAbsolutePath()) < 10)
				return null;
			else
				return result;
		} catch (IOException ex) {
			Log.w("Script", "receiveImage : "+ex);
			return null;
		}
	}

	private File getRealResourceFile(String fileName, String supposedFolder) {
		File f;
		if (fileName.startsWith("/") || fileName.contains(":"))
			f = new File(fileName);
		else
			f = new File(main.getDataFolder() + "/" + supposedFolder, fileName);
		if (!f.exists()) {
			showPopup(main.getString("missing", f.getPath()));
			return null;
		}
		return f;
	}

	public void exit() {
		main.finish();
	}

	public String run() throws InterruptedException {
		// override me !
		return null;
	}

	public void onActivityResult(Intent data) {
		lastResult = data;
		if(data!=null)
			Log.d("Script", "onActivityResult("+data.getExtras()+") : notify()");
		else
			Log.d("Script", "onActivityResult(no data) : notify()");
		synchronized (this) {
			notify();
		}
	}
}
